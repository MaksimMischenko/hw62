import React, {Fragment} from 'react';
import Header from "../Header/Header";
import './AboutUs.css';
import Footer from "../Footer/Footer";

const AboutUs = props => {
    return (
        <Fragment>
            <Header/>
            <div className="AboutUs container">
                <div className="AboutTxt">
                    <h3>About Us</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aut consequatur distinctio
                        dolorem dolores eum ex fuga hic inventore, ipsa itaque, maiores minima placeat, recusandae
                        temporibus vitae voluptate. Dignissimos earum illum odio sed! Beatae enim, expedita! Amet
                        aspernatur delectus dolor dolores, doloribus earum, eius id minus officia porro quae qui unde
                        voluptate. Asperiores iste magni nostrum officia quaerat repudiandae soluta temporibus!
                        Architecto ea inventore necessitatibus optio possimus qui tenetur ullam?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad adipisci, alias
                        architecto beatae dolore dolores doloribus error explicabo hic iste nam nostrum obcaecati sit
                        suscipit unde veniam vero? Iure.</p>
                </div>
            </div>
            <div className="contact">
                <div className="container">
                    <h3>CONTACT US TODAY</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure

                    </p>
                    <button className="btn">Get in touch</button>
                </div>
            </div>
            <Footer/>
        </Fragment>
    )
};

export default AboutUs;