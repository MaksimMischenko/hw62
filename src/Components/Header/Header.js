import React from 'react';
import {Link} from "react-router-dom";
import './Header.css';

const Header = props => {
    return(
        <header className="Header">
            <div className="container clearfix">
                <div className="Logo">
                    <Link to="/">WWW</Link>
                </div>
                <nav className="Nav">
                    <ul>
                        <li><Link to='/'>Home</Link></li>
                        <li><Link to='/services'>Services</Link></li>
                        <li><Link to='/aboutUs'>About us</Link></li>
                    </ul>
                </nav>
            </div>

        </header>
    )
};

export default Header;