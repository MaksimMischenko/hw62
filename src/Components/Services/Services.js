import React, {Fragment} from 'react';
import './Services.css';
import Header from "../Header/Header";
import Footer from "../Footer/Footer";


const Services = () => {
    return (
        <Fragment>
            <Header/>
            <div className='best-services'>
                <div className='txt'>
                    <h2>WE GIVE BEST SERVICES</h2>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum
                        deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                        providen</p>
                </div>
                <div className='items container'>
                    <div className='item item-1'>
                        <div className='inner'>
                            <i className="fas fa-road"></i>
                            <h4>NEW WAY OF VISION</h4>
                            <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet
                                ut et voluptates</p>
                        </div>
                    </div>
                    <div className='item item-1'>
                        <div className='inner'>
                            <i className="fas fa-home"></i>
                            <h4>FRESH IDEAS</h4>
                            <p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus
                                maiores alias</p>
                        </div>
                    </div>
                    <div className='item item-2'>
                        <div className='inner'>
                            <i className="fas fa-leaf"></i>
                            <h4>STRONG FOUNDATIONS</h4>
                            <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe
                                evennet</p>
                        </div>
                    </div>
                    <div className='item item-3'>
                        <div className='inner'>
                            <i className="fas fa-users"></i>
                            <h4>GREAT SUPPORT</h4>
                            <p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus
                                maiores</p>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
        </Fragment>
    )
};

export default Services;