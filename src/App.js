import React, {Component} from 'react';
import {Route, Switch, BrowserRouter} from "react-router-dom";
import HomePage from './Containers/HomePage/HomePage';
import Services from './Components/Services/Services';
import './App.css';
import AboutUs from "./Components/AboutUs/AboutUs";

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path='/' exact component={HomePage}/>
                    <Route path='/services' component={Services}/>
                    <Route path='/aboutus' component={AboutUs}></Route>
                </Switch>
            </BrowserRouter>
        );
    }
}
